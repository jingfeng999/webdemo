import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Auther : jingfeng999
 * @Date : 2021/9/10 15:36
 * @Description: 恋上数据结构与算法
 * @Version: 1.0
 */
public class GreetingServer extends Thread {
    private ServerSocket serverSocket;

    public GreetingServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void run() {
        Socket client = null;
        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            while (true) {
                System.out.println("Waiting for client on port " +
                        serverSocket.getLocalPort() + "...");
                client = serverSocket.accept();
                //获取客户端数据,
                InputStream inputStream = client.getInputStream();
                br = new BufferedReader(new InputStreamReader(inputStream));
                String s = null;
                while((s = br.readLine()) != null){
                    if(s.substring(0,3).equals("GET")) break; //读取到带有请求参数的那一行
                }
                System.out.println(s);
                String[] s1 = s.split(" ");
                String[] s2 = s1[1].split("\\?");
                //标识出是什么操作
                String operation = s2[0].substring(1);
                System.out.println(operation);
                boolean isRequestSum = true;
                switch (operation) {
                    case "add": {
                        isRequestSum = true;
                    }
                    break;
                    case "mult": {
                        isRequestSum = false;
                    }
                }
                //找出参数,计算
                int result = 1;
                String[] s3 = s2[1].split("&");
                for (String ss : s3) {
                    String substring = ss.substring(ss.indexOf("=") + 1);
                    int i = Integer.parseInt(substring);
                    if (isRequestSum) {
                        result += i;
                    } else {
                        result *= i;
                    }
                }
                if (isRequestSum) result--;
                System.out.println(result);
                //输出
                OutputStream outputStream = client.getOutputStream();
                bw = new BufferedWriter(new OutputStreamWriter(outputStream));
                String ss = "<html><head></head><body>" +
                        "<h1>" + result + "</h1>" +
                        "" +
                        "</body></html>";
                bw.write(ss);
                bw.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert client != null;
                client.close();
                assert bw != null;
                bw.close();
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        int port = 80;
        try {
            Thread t = new GreetingServer(port);
            t.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
